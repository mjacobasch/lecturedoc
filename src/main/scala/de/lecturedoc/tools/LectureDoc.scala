/*
 * (c) 2013 Michael Eichberg et al.
 * https://bitbucket.org/delors/lecturedoc
 * 
 * See LICENSE and Actuarius-LICSENSE for license details. 
 */
package de.lecturedoc
package tools

import parser._

import scala.io.Source
import java.util.Date
import java.io.OutputStream
import java.io.InputStream
import java.io.IOException
import scala.io.Codec
import java.text.DateFormat
import java.nio.file.FileSystems
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.WatchKey
import java.nio.file.StandardWatchEventKinds.ENTRY_CREATE
import java.nio.file.StandardWatchEventKinds.OVERFLOW
import java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY

/**
  * Contains a main method that simply reads everything from stdin, parses it as LectureDoc and
  * prints the result to stdout.
  */
object LectureDoc extends Transformer {

    /**
      * Reads a LectureDoc file from the given InputStream and writes the generated HTML5 file to the
      * OutputStream.
      *
      * @param complete If true, a complete HTML document is created; otherwise a fragment is created.
      * @param lastModified The date when the underlying md file was lastModified.
      */
    def process(in: java.io.InputStream, out: java.io.OutputStream, complete: Boolean, lastModified: Date) {

        val printStream = new java.io.PrintStream(out, true, "UTF-8")

        def println(s: String) { printStream.println(s) }

        def print(s: String) { printStream.print(s) }

        //read from system input stream
        val input = Source.fromInputStream(in)(Codec.UTF8).mkString

        //run that string through the transformer trait's apply method
        try {
            val output = apply(input)

            if (complete) {
                // TODO [Refine] replace the hard coded HTML using a template language (e.g., velocity)
                // TODO [Refine] make it possible to add additional header fragments     
                val thisClassLoader = this.getClass().getClassLoader()
                println("<!DOCTYPE html><html><head><meta charset='utf-8'><meta name=generator content='LectureDoc'>")
                val headerFragment = Option(thisClassLoader.getResourceAsStream("Library/header.fragment.html")).
                    map(Source.fromInputStream(_).mkString).
                    getOrElse("<!-- No header fragment found (\"Library/header.fragment.html\"). -->")
                println(headerFragment)
                println("</head><body data-ldjs-last-modified=\""+lastModified.getTime()+"\">")
                println("<div id='body-content'>")
                print(output)
                println("</div>")
                val footerFragment = Option(thisClassLoader.getResourceAsStream("Library/footer.fragment.html")).
                    map(Source.fromInputStream(_).mkString).
                    getOrElse("<!-- No footer fragment found (\"Library/footer.fragment.html\"). -->")
                println(footerFragment)
                println("</body></html>")
            }
            else {
                print(output)
            }
        }
        catch {
            case e: Exception ⇒ {
                println("Conversion failed: "+e.getLocalizedMessage())
                System.out.println("Conversion failed (see generated file for details!)")
            }
        }
    }

    private lazy val helpMessage =
        withResource(
            this.getClass().getResourceAsStream("LectureDocHelp.txt"))(
                Source.fromInputStream(_).mkString)

    private def printUsageAndExit() {
        println(helpMessage)
        System.exit(-2)
    }

    private def printErrorAndExit(errorMessage : String) {
        System.err.println(errorMessage)
        println(helpMessage)
        System.exit(-1)
    }
    
    def main(args: Array[String]): Unit = {
        if (args.length > 3) printUsageAndExit();

        val complete = args.contains("--complete")
        val continuous = args.contains("--continuous")

        if (continuous) {
            // continuous mode. 
            // I.e., we watch a directory for changes and create the new LectureDoc files when needed
            import java.nio.file._
            import java.nio.file.StandardWatchEventKinds._
            import scala.collection.JavaConversions._

            val path: Path =
                if (complete && args.length == 3) { Paths.get(args(2)); }
                else if (!complete && args.length == 2) { Paths.get(args(1)); }
                else { Paths.get(System.getProperty("user.dir")) }

            if (!(Files.isWritable(path) && Files.isDirectory(path))) printUsageAndExit()

            println("Watching directory: "+path.toAbsolutePath()+" for changes.")

            val watchService = FileSystems.getDefault().newWatchService();
            path.register(watchService, ENTRY_CREATE, ENTRY_MODIFY);
            var watchKey: WatchKey = null
            do {
                watchKey = try { watchService.take() } catch { case e: InterruptedException ⇒ return }
                for {
                    event ← watchKey.pollEvents()
                    if event.kind() ne OVERFLOW
                    file = path.resolve(event.context().asInstanceOf[Path])
                    fileName = file.toString
                    if fileName.endsWith(".md")
                } {
                    // Convert the updated/new lecture doc file
                    val lastModified = Files.getLastModifiedTime(file).toMillis()
                    val date = new java.util.Date(lastModified)
                    println("Converting file: "+fileName+" last modified: "+DateFormat.getDateTimeInstance().format(date))
                    val newFileName = fileName.substring(0, fileName.length() - 3)+".html"
                    val newFile = Paths.get(newFileName)
                    Files.deleteIfExists(newFile);

                    var in: InputStream = null
                    var out: OutputStream = null

                    try {
                        in = Files.newInputStream(file)
                        out = Files.newOutputStream(newFile)
                        process(in, out, complete, date)
                        Files.setLastModifiedTime(newFile, java.nio.file.attribute.FileTime.fromMillis(lastModified))
                    }
                    catch {
                        case e: IOException ⇒
                            System.err.println("Error while converting: "+file+" => "+newFile+" ("+e.getMessage()+")")
                    }
                    finally {
                        if (in != null)
                            in.close()
                        if (out != null)
                            out.close();
                    }
                }
            } while (watchKey.reset())

            System.err.println("Watching the path: "+path+" was aborted.")
        }
        else {
            // Standard mode
            if (args.length > 1) printErrorAndExit("Unsupported arguments: "+args)
            process(System.in, System.out, complete, new java.util.Date() /*last-modified*/ )
        }
    }

}

/*
 * (c) 2013 Michael Eichberg et al.
 * https://bitbucket.org/delors/lecturedoc
 * 
 * See LICENSE and Actuarius-LICSENSE for license details. 
 */
package de.lecturedoc
package parser

/**
  * Parses single lines into tokens.
  *
  * Markdown lines are differentiated by their beginning.
  * These lines are then organized in blocks by the BlockParsers.
  */
trait LineParsers extends InlineParsers {

    /////////////////////////////////
    // Link definition pre-parsing //
    /////////////////////////////////

    /**
      * The Start of a link definition: the id in square brackets, optionally indented by three spaces
      */
    def linkDefinitionId: Parser[String] =
        """ {0,3}\[""".r ~> markdownText(Set(']'), true) <~ ("]:" ~ ows) ^^ { _.trim.toLowerCase }

    /**
      * The link url in a link definition.
      */
    def linkDefinitionUrl: Parser[String] =
        (elem('<') ~> markdownText(Set('>'), true) <~ '>' ^^ { _.mkString.trim }) |
            (markdownText(Set(' ', '\t'), true) ^^ { _.mkString })

    /**
      * The title in a link definition.
      */
    def linkDefinitionTitle: Parser[String] =
        ows ~> ("""\"[^\n]*["]""".r |
            """\'[^\n]*\'""".r |
            """\([^\n]*\)""".r) <~ ows ^^ { s ⇒ s.substring(1, s.length - 1) }

    /**
      * A link definition that later gets stripped from the output.
      * Either a link definition on one line or the first line of a two line link definition.
      */
    def linkDefinitionStart: Parser[(LinkDefinitionStart, Option[String])] =
        linkDefinitionId ~ linkDefinitionUrl ~ opt(linkDefinitionTitle) ^^ { case i ~ u ~ t ⇒ (new LinkDefinitionStart(i, u), t) }

    //////////////////////////////////////////
    // Lines for XML Block tokenizing       //
    //////////////////////////////////////////

    /**
      * A line that starts an xml block: an opening xml element fragment.
      */
    def xmlBlockStartLine: Parser[String] = guard('<' ~ xmlName) ~> rest
    /**
      * A line that ends an xml block: a line starting with an xml end tag
      */
    def xmlBlockEndLine: Parser[String] = guard(xmlEndTag) ~> rest
    /**
      * A line not starting with an xml end tag
      */
    def notXmlBlockEndLine: Parser[String] = not(xmlEndTag) ~> rest

    //////////////////////////////
    // Markdown line tokenizing //
    //////////////////////////////

    /**
      * Parses the line under a setext style level 1 header: =====
      */
    val setextHeader1: Parser[SetExtHeaderLine] = """=+([ \t]*)$""".r ^^ { new SetExtHeaderLine(_, 1) }

    /**
      * Parses the line under a setext style level 2 header: -----
      */
    val setextHeader2: Parser[SetExtHeaderLine] = """((\-)+)([ \t]*)$""".r ^^ { new SetExtHeaderLine(_, 2) }

    /**
      * Parses headers of the form: ### header ###
      */
    val atxHeader: Parser[AtxHeaderLine] = """#+""".r ~ rest ^^ {
        case prefix ~ payload ⇒ new AtxHeaderLine(prefix, payload)
    }

    /**
      * Parses statement of the form: ^statement
      */
    val statement: Parser[StatementLine] = """\^( )?""".r ~ rest ^^ {
        case prefix ~ payload ⇒ new StatementLine(prefix, payload)
    }

    /**
      * Parses a horizontal rule.
      */
    val ruler: Parser[MarkdownLine] = """ {0,3}(((-[ \t]*){3,})|((\*[ \t]*){3,}))$""".r ^^ { new RulerLine(_) }

    /**
      * Matches a line starting with up to three spaces, a '>' and an optional whitespace.
      * (i.e.: the start or continuation of a block quote.)
      */
    val blockquoteLine: Parser[BlockQuoteLine] = """ {0,3}\>( )?""".r ~ rest ^^ {
        case prefix ~ payload ⇒ new BlockQuoteLine(prefix, payload)
    }

    /**
      * A line that starts an unordered list item.
      * Matches a line starting with up to three spaces followed by an asterisk, a space, and any whitespace.
      */
    val uItemStartLine: Parser[UItemStartLine] = (""" {0,3}[\*\+-] [\t\v ]*""".r) ~ rest ^^ {
        case prefix ~ payload ⇒ new UItemStartLine(prefix, payload)
    }

    /**
      * A line that starts an ordered list item.
      * Matches a line starting with up to three spaces followed by a number, a dot and a space, and any whitespace
      */
    val oItemStartLine: Parser[OItemStartLine] = (""" {0,3}[0-9]+\. [\t\v ]*""".r) ~ rest ^^ {
        case prefix ~ payload ⇒ new OItemStartLine(prefix, payload)
    }

    /**
      * Accepts an empty line. (A line that consists only of optional whitespace or the empty string.)
      */
    val emptyLine: Parser[MarkdownLine] = """([ \t]*)$""".r ^^ { new EmptyLine(_) }

    /**
      * Matches a code example line: any line starting with four spaces or a tab.
      */
    val codeLine: Parser[CodeLine] = ("    " | "\t") ~ rest ^^ {
        case prefix ~ payload ⇒ new CodeLine(prefix, payload)
    }

    /**
      * A fenced code line. Can be the start or the end of a fenced code block
      */
    val fencedCodeLine: Parser[FencedCode] = """\`{3,}[\t\v ]*$""".r ^^ {
        case prefix ⇒ new FencedCode(prefix)
    }

    /**
      * Matches the start of a fenced code block with additional language token:
      * up to three spaces, three or more backticks, whitespace, an optional
      * language token, optional whitespace
      */
    val extendedFencedCodeLine: Parser[ExtendedFencedCode] = """\`{3,}[\t\v ]*""".r ~ """[\w_-]+[\t\v ]*""".r ^^ {
        case prefix ~ languageToken ⇒ new ExtendedFencedCode(prefix, languageToken)
    }

    def customDataAttributes: Parser[List[CustomDataAttribute]] =
        customDataAttribute ~ opt(';' ~> customDataAttributes) ^^ {
            case cda ~ Some(cdas) ⇒ cda +: cdas
            case cda ~ None       ⇒ List(cda)
        }

    def customDataAttribute: Parser[CustomDataAttribute] =
        """\w[\w\-_]*""".r ~ ':' ~ """\w+""".r ^^ {
            case key ~ _ ~ value ⇒ CustomDataAttribute(key, value)
        }

    /**
      * A section line starts with three colons and optionally a class.
      */
    val sectionStartLine: Parser[SectionStart] =
        """\+~""".r ~
            opt("[" ~> markdownText(Set(']'), true) <~ "]") ~
            opt("""([\w][\w\-_]*[\t\v ]*)+""".r) ~
            opt('{' ~> customDataAttributes <~ '}') ^^ {
                case prefix ~ sectionTitle ~ sectionClass ~ customDataAttributes ⇒
                    new SectionStart(prefix, sectionTitle, sectionClass, customDataAttributes)
            }

    /**
      * A section end line starts with three colons. Can be the start or the end.
      */
    val sectionEndLine: Parser[SectionEnd] = """~\+[\t\v ]*$""".r ^^ {
        case prefix ⇒ new SectionEnd(prefix)
    }

    /**
      * Matches any line. Only called when all other line parsers have failed.
      * Makes sure line tokenizing does not fail and we do not loose any lines on the way.
      */
    val otherLine: Parser[OtherLine] = rest ^^ { new OtherLine(_) }

    ///////////////////////////////////////////////////////////////
    // combined parsers for faster tokenizing based on lookahead //
    ///////////////////////////////////////////////////////////////
    /**
      * First tries for a setext header level 2, then for a ruler.
      */
    val setext2OrRuler: Parser[MarkdownLine] = setextHeader2 | ruler
    /**
      * First tries for a ruler, then for an unordered list item start.
      */
    val rulerOrUItem: Parser[MarkdownLine] = ruler | uItemStartLine
    /**
      * First tries if the line is empty, if not tries for a code line.
      */
    val emptyOrCode: Parser[MarkdownLine] = emptyLine | codeLine

    /**
      * Parses one of the fenced code lines
      */
    val fencedCodeStartOrEnd: Parser[MarkdownLine] = fencedCodeLine | extendedFencedCodeLine

}


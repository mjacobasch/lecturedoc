/*
 * (c) 2013 Michael Eichberg et al.
 * https://bitbucket.org/delors/lecturedoc
 * 
 * See LICENSE and Actuarius-LICSENSE for license details. 
 */
package de.lecturedoc
package parser

/**
  * This is the Transformer that uses the other parsers to transform LectureDoc into HTML 5.
  * 
  * Mix this trait in if you want more control over the output (like switching verbatim xml on/off or using
  * different opening/closing tags for the output).
  */
trait Transformer {

    /**
      * Overwrite this method to return a custom decorator if you want modified output.
      */
    def deco(): Decorator = Decorator

    private object lineTokenizer extends LineTokenizer {
        override def allowXmlBlocks() = Transformer.this.deco().allowVerbatimXml()
    }
    
    private object blockParser extends BlockParsers {
        override def deco() = Transformer.this.deco()
    }

    /**
      * This is the method that turns LectureDoc source into HTML 5.
      */
    def apply(s: String) = {
        //first, run the input through the line tokenizer
        val lineReader = lineTokenizer.tokenize(s)
        //then, run it through the block parser
        blockParser(lineReader)
    }
}

/**
  * Simple standalone LectureDoc transformer.
  * 
  * Use this if you simply want to transform a block of LectureDoc without any special options.
  * {{{
  * val input:String = ...
  * val xhtml:String = new LectureDocTransformer()(input)
  * }}}
  *
  * Note that LectureDoc and hence this class is not thread-safe; this is because it is based on Scala Parser 
  * Combinators which are not thread-safe.
  */
class LectureDocTransformer extends Transformer

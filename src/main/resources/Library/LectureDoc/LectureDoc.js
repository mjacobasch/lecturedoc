/*
   Copyright 2012 Michael Eichberg et al - www.michael-eichberg.de

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

// TODO Add some high-level document regarding the fundamental architecture/flow
// TODO Add a posibility to "write/type text" on a slide
// TODO Create the overview based on the set of current slides (to make annotations visible!) 
// TODO Add support for persistent per slide notes.
// TODO Add multitouch support.
// TODO Add support for the management of server-side notes

/*
	The following script turns a browser in a fully-fledged presentation program (if 
	the browser has a presentation mode/a chromeless mode).
	The basic idea is that we render a standard HTML document that uses some
	predefined CSS classes as a set of slides. 
	
	The idea is that all SECTION elements where the CLASS attribute contains "slide"
	are rendered as single slides and the user can change the slides using the
	standard controls (left, right keys etc.) known from other presentation programs.

	@author Michael Eichberg
*/

var LectureDoc = (function () {

   /** 
		Converts, e.g., a live NodeList into an array. 
	*/

   function toArray(nodeList) {
      return Array.prototype.slice.call(nodeList);
   }

   // 
   // The following variables contain immutable state.
   // 

   /**
	 	A copy of the original document body.
	 	
	 	@final
	*/
   var theDocumentBody;

   /**
		The list of all SECTION elements where the CLASS attribute contains "slide".
		
		A section that represents a slide must not contain other SECTION elements
		where the CLASS attribute also contains "slide". Hence, using a SECTION element
		with any other class is still possible. 
		
		If a slide contains another slide, the result (rendering of the slide) is not 
		defined and may change without further notice.
		
		@final
	*/
   var theSlides;

   //
   // The following variables contain mutable state that is related to the presentation
   // as a whole.
   //

   /**
		The id (number) of the current slide that is shown.
	*/
   var currentSlide = undefined;

   /**
    * When properly initialized, this is a list of lists of links to aside elements
    * contained on the presentation's slides. The indexes of slideAsides correspond
    * to the slide indexes where the links contained in the sublist are found.
    */
   var slideAsides = undefined;

   function bodyContent() {
      return document.getElementById('body-content');
   }

   var helpNode = function () {
      var node = document.createElement('div');
      node.id = 'ldjs-help';
      node.innerHTML = "Help has not been loaded yet.";
      return node;
   }();

   /**
    * Generates a list of lists of links to asides elements contained in slides, where
    * each list of links is accessible via its parent slide's index.
    * @param {Array of DOM Elements} theSlides the list of slides to generate the aside
    * links for
    * @return a list of lists of A elements
    */

   function initializeAsides(theSlides) {
      var asidesList = [];

      function inBody(element) {
         return element.dataset.title;
      }
      theSlides.forEach(function (slide) {
         var asides = toArray(slide.querySelectorAll(
            ".section-body>aside[data-title]"));
         asidesList.push(asides.map(function (aside) {
            var asideLink = document.createElement("a");
            asideLink.className = "ldjs-aside-link ldjs-menubar-menu-button";
            asideLink.appendChild(document.createTextNode(aside.dataset.title)); // innerText is not supported by Firefox
            asideLink.addEventListener("click", function () {
               if (window.getComputedStyle(aside).getPropertyValue(
                  "visibility") === 'hidden') {
                  aside.style.visibility = 'visible';
                  asideLink.firstChild.appendData(" ✦");
               } else {
                  aside.style.visibility = 'hidden';
                  var linkText = asideLink.firstChild;
                  linkText.deleteData(linkText.data.length - 2, 2);
               };
            }, false);
            return asideLink;
         }));
      });
      return asidesList;
   }

   function analyzeDocument() {
      theDocumentBody = document.body.cloneNode(true);
      theSlides = toArray(document.querySelectorAll("section.slide"));
      slideAsides = initializeAsides(theSlides);
   }


   /*	
    * Closes the print dialog and (depending on the parameter) opens the browser print dialog or not.
    * @param {boolean} print if true the browser print dialog will open
    */

   function closePrintDialog(print) {
      var printMessage = document.getElementById("ldjs-print-dialog");

      if (printMessage) {
         bodyContent().removeChild(printMessage);
      }

      removeOverlayLayer();

      if (print === true) {
         window.print();
      }

      isPrintDialogActive = false;
   }


   function openPrintDialog() {
      if (isPrintDialogActive) return; // If there is already an open print dialog it should not create another printDialog over it

      addOverlayLayer("white", "modal");
      document.getElementById("ldjs-overlay-layer").onclick = function () {
         closePrintDialog(false);
      };
      document.getElementById("ldjs-overlay-layer").focus(); //focuses the overlay, so that the menu can't be clicked while the PrintDialog is open
      var message = "<h1>Tips for printing in LectureDoc</h1>";
      var button = "<div>Continue printing</div>";
      var hints =
         "<ul><li>To print a <b>single slide</b>, navigate to it in <b>presentation mode</b>.</li>" +
         "<li>To print a <b>script-like document</b>, switch to <b>standard document mode</b>.</li>" +
         "<li>To print <b>several slides</b> per page, switch to <b>light table mode</b> an select a zoom setting from the menu bar.</li>" +
         "<li>To print <b>one slide</b> per page, e.g. for creating a PDF, switch to <b>continuous mode</b>.</li></ul>" +
         "<p>Click outside of this dialog to abort print.</p>";

      if (isInLightTableMode()) {
         message +=
            "<p>Printing in this mode is only supported in <b>landscape</b> orientation.</p>";

      } else if (isInStandardDocumentMode()) {
         message +=
            "<p>Printing in this mode is only supported in <b>portrait</b> orientation.</p>";

      } else if (isInContinuousLayoutMode()) {
         message +=
            "<p>Printing in this mode is only supported in <b>landscape</b> orientation.</p>";

      } else if (isInPresentationMode()) {
         message +=
            "<p>Printing in this mode is only supported in <b>landscape</b> orientation.</p>";

      } else { // if someone uses strg+p in a not supported mode
         message += "<p>Printing in this mode is currently <b>not supported</b>.</p>";

      }
      message += hints + button;

      addMessageBox("ldjs-print-dialog", message, function () {
         closePrintDialog(true);
      }, 0);
      isPrintDialogActive = true;

   }

   /**
    * Closes a modal dialog previously opened via openModalDialog.
    *
    * @see openModalDialog
    *
    */

   function closeModalDialog() {
      var modalDialog = document.getElementById("ldjs-modal-dialog");

      if (modalDialog) {
         bodyContent().removeChild(modalDialog);
      }

      removeOverlayLayer();
      isModalDialogActive = false;
   }

   /**
    * Opens a modal dialog. Any controls that do not directly belong to
    * this dialog are disabled as long as this dialog is open.
    *
    * @param message the message to show in this dialog
    *
    * @see closeModalDialog
    *
    */

   function openModalDialog(message) {
      if (isModalDialogActive) return;

      addOverlayLayer("white", "modal");
      document.getElementById("ldjs-overlay-layer").focus(); //focuses the overlay, so that the menu can't be clicked while the dialog is open
      addMessageBox("ldjs-modal-dialog", message, closeModalDialog, 0);
      isModalDialogActive = true;
   }

   /**
    * Opens a modal dialog to make the user aware of the menu button.
    *
    */

   function showMenuHint() {
      openModalDialog("<p><b>The menu button is located<br>" +
         "in the bottom right corner.</b></p>" +
         "<p>Click on this window to hide it.</p>");
   }

   /**
		Initialization that is independent of the current mode (presentation, continuous, light table,...),
		but which makes sense, if and only if, we have at least one slide.
	*/

   function enableLectureDoc() {
      document.addEventListener(
         "keyup",
         function (event) {
            console.log(event);
            var target = event.target.tagName;
            if (target === "TEXTAREA" ||
               target === "INPUT" ||
               event.altKey ||
               event.ctrlKey ||
               event.metaKey ||
               event.shiftKey ||
               isPrintDialogActive ||
               isModalDialogActive) {
               return;
            }

            if (event.keyCode == 77 /*m*/ ) {
               // Select how the slides are presented. 
               toggleSubmenu('ldjs-menubar-submenu-modes', "ldjs-menubar-modes-button");
            }


         },
         true
      );

      document.addEventListener("keydown", function (event) {
         if (event.ctrlKey && event.keyCode == 80) { // ctrl+P
            event.preventDefault(); // do not show default print dialog
            openPrintDialog();
         }
      }, false);

   }

   /**
    * @param {Integer} slide - The id of the slide (zero-based) that should be shown when the
    * 					presentation mode is first launched. If the id is larger than the
    * 					number of slides, the id is ignored and the first slide will be shown.
    */

   function setupPresentationMode(slide) {
      addSlideNumbers(theSlides);

      // Register event handlers to make it possible to steer and manipulate the 
      // presentation
      //
      console.log("Activating presentation controls.");

      activateLaserPointerSupport();

      theSlides.forEach(addCanvasLayer);

      // We don't want to display slides directly after initializing,
      // so just remember which slide we're supposed to load
      currentSlide = slide;
   }

   /**
		Adds slide numbers to all extracted slides.
		
		FIXME The size of the slide number is not the same on all slides.
	*/

   function addSlideNumbers(theSlides) {
      for (var s = 0; s < theSlides.length; s++) {
         var slideNumber = document.createElement("div");
         slideNumber.innerHTML = s + 1;
         slideNumber.className = "ldjs-slide-number";
         theSlides[s].firstChild.appendChild(slideNumber); // TODO make the .firstChild selector more stable!
      }
   }


   /**
		Enables the virtual laser pointer. The virtual laser pointer is basically, 
		a DIV element that is added as the first element to the BODY element and
		which uses fixed positioning.
	*/

   function activateLaserPointerSupport() {
      var theLaserPointer = document.createElement("div");
      theLaserPointer.id = "ldjs-laserpointer";

      document.body.addEventListener(
         'mousemove',
         function (event) {
            if (event.ctrlKey) {
               // When the slide was changed the laser pointer may belong to
               // the wrong section element; i.e. a section element belonging
               // to a slide that is not shown; repair if necessary.
               if (theLaserPointer.parentNode !== document.body) {
                  document.body.insertBefore(theLaserPointer, document.body.firstChild);
               }
               theLaserPointer.style.left = event.clientX + "px";
               theLaserPointer.style.top = event.clientY + "px";
               theLaserPointer.style.visibility = "visible";
               event.preventDefault();
            } else {
               theLaserPointer.style.visibility = "hidden";
            }
         },
         false);
   }

   /**
		Returns a function that – when called later on – jumps to the specified slide.
		
		@param {Integer} targetSlide - The valid id ([0...#NumberOfSlides]) of a slide.
		@return A function that - when called later on - causes the presentation to jump 
				to the specified slide.
	*/

   function jumpToSlide(targetSlide) {
      return function () {
         currentSlide = targetSlide;
         if (isInPresentationMode() || isInLightTableMode()) {
            displaySlide();
         } else {
            var slide = bodyContent().querySelectorAll("section.slide")[currentSlide];
            slide.scrollIntoView(true);
         }
      };
   }

   /**
		Adds a canvas layer to a slide. 
		
		Note: the canvas layer is not added to the BODY element, because we want to preserve a 
		the contents of the canvas layer when the slide is changed.
	*/

   function addCanvasLayer(slide) {
      var canvas = document.createElement("div");
      canvas.id = "ldjs-canvas";
      slide.firstChild.insertBefore(canvas, slide.firstChild.firstChild); // TODO make the selector more stable; i.e., replace firstChild by section>div.section-body or something similar

      var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
      canvas.appendChild(svg);

      var strokeColor = "black";
      var x = -1; // stores the x position of the mouse (when we receive a mousemove event)
      var y = -1; // stores the y position of the mouse (when we receive a mousemove event)
      // The mouse move event listener is added to the slide and not the canvas
      // because the canvas(svg) is configured to not intercept mouse events to
      // enable users to,e.g., still click on links even if the canvas is shown.
      // CSS Property: 	svg{ pointer-events: none }.
      slide.addEventListener(
         "mousemove",
         function (event) {
            if (event.altKey && window.getComputedStyle(canvas).getPropertyValue(
               "visibility") === 'visible') {
               //if(event.buttons === 1){
               var new_x = event.clientX - canvas.parentNode.offsetLeft;
               var new_y = event.clientY - canvas.parentNode.offsetTop;
               var scale = parseFloat(bodyContent().dataset.ldjsSlideZoom) || 1;
               new_x = new_x / scale;
               new_y = new_y / scale;
               if (x >= 0 && (new_x !== x || new_y !== y)) {
                  var line = document.createElementNS("http://www.w3.org/2000/svg",
                     "line");
                  line.setAttribute("stroke", strokeColor);
                  line.setAttribute("stroke-width", "3");
                  line.setAttribute("x1", x);
                  line.setAttribute("y1", y);
                  line.setAttribute("x2", new_x);
                  line.setAttribute("y2", new_y);
                  svg.appendChild(line);
               }
               x = new_x;
               y = new_y;
            } else {
               x = -1;
               y = -1;
            }
            return true;
         }
      );

      var menu = document.createElement("div");
      menu.id = "ldjs-canvas-menu";
      canvas.appendChild(menu);

      var clearCanvas = document.createElement("div");
      clearCanvas.id = "ldjs-canvas-menu-clear-canvas";
      clearCanvas.appendChild(document.createTextNode("✗"));
      clearCanvas.onclick = function () {
         while (svg.hasChildNodes()) {
            svg.removeChild(svg.firstChild);
         }
      };
      menu.appendChild(clearCanvas);

      var blackStroke = document.createElement("div");
      blackStroke.id = "ldjs-canvas-menu-black-stroke";
      blackStroke.appendChild(document.createTextNode("◉"));
      blackStroke.onclick = function () {
         strokeColor = "black";
      };
      menu.appendChild(blackStroke);

      var yellowStroke = document.createElement("div");
      yellowStroke.id = "ldjs-canvas-menu-yellow-stroke";
      yellowStroke.appendChild(document.createTextNode("◉"));
      yellowStroke.onclick = function () {
         strokeColor = "yellow";
      };
      menu.appendChild(yellowStroke);

      var redStroke = document.createElement("div");
      redStroke.id = "ldjs-canvas-menu-red-stroke";
      redStroke.appendChild(document.createTextNode("◉"));
      redStroke.onclick = function () {
         strokeColor = "red";
      };
      menu.appendChild(redStroke);
   }


   //
   // The following variables contain state that is only relevant while a particular
   // slide is shown. When the slide changes, the following variables are reset to
   // their default values that are given next.
   //

   /**
	 	Array of timeouts (integer values) that should be cancelled, when the slide is changed
	 */
   var timeouts = [];
   var enteredSlideNumber = undefined;
   var isOverlayActive = false;
   var isHelpShown = false;
   var lightTableSlidesPerRow = 4;
   var isPrintDialogActive = false;
   var isModalDialogActive = false;
   var isMenuKnown = false;

   /**
		Resets all variables that are related to showing a particular slide to their 
		default state.
	*/

   function clear() {
      resetZoom();
      bodyContent().innerHTML = ""; // clear the page (prepare for rendering the next slide)
      document.body.className = "";

      hideAllMenuButtons();
      showDefaultMenuButtons();
      disableButton("ldjs-menubar-asides-button");

      timeouts.forEach(function (timeout) {
         clearTimeout(timeout);
      });
      timeouts = [];

      enteredSlideNumber = undefined;
      isOverlayActive = false;
      isHelpShown = false;
   }

   function displaySlide() {
      clear();
      document.body.classList.add("ldjs-slide");

      showMenuButtons(["ldjs-menubar-asides-button",
                   'ldjs-menubar-whiteout-button',
       'ldjs-menubar-blackout-button',
       'ldjs-menubar-canvas-button',
       "ldjs-menubar-scrolling-overview-button",
       'ldjs-menubar-list-of-slides-button',
       'ldjs-menubar-zoom-button']);

      if (currentSlide < theSlides.length && slideAsides[currentSlide].length > 0) {
         enableButton("ldjs-menubar-asides-button");
      }

      // Activate keyboard presentation controls. (Recall that an event listener that is registered more than once is automatically discarded.)
      document.addEventListener("keyup", handlePresentationKeyEvent, true);

      if (currentSlide < theSlides.length) {
         console.log("Rendering slide: " + currentSlide);
         bodyContent().appendChild(theSlides[currentSlide]);
      } else if (currentSlide > theSlides.length) {
         // Show the "first slide". 
         // 
         // Implementation note: Between the last time the presentation was shown and this
         // time the number of slides may have changed. If the number was reduced, we
         // just start with the first slide and show a corresponding message.
         console.log("Trying to reference missing slide. Rendering first slide.");
         bodyContent().appendChild(theSlides[0]);
         addMessageBox(
            "ldjs-presentation-reset-warning",
            "The number of slides has changed.<br>Restarting the presentation with the first slide.",
            undefined,
            5000,
            "ldjs-message ldjs-message-error");
      } else {
         // TODO Improve the look of the "presentation finished." page.
         bodyContent().innerHTML =
            "<div id='ldjs-black-layer'><div class='ldjs-presentation-finished'>Presentation finished.<br /> Press f to go to the first page.</div></div>";
      }

      // We persist the id of the current slide to enable continuing the presentation
      // – after restart – where we left off...
      try {
         window.localStorage.setItem("lecturedoc-currentSlide-" + window.location.toString(),
            currentSlide);
      } catch (err) {
         console.log("Cannot store the information about the current slide: " + err +
            ".");
      }

      // restore slide zoom if set
      if (bodyContent().dataset['ldjsSlideZoom']) {
         setPresentationZoomFactor(parseFloat(bodyContent().dataset['ldjsSlideZoom']));
      }

      if (!isMenuKnown) {
         showMenuHint();
         isMenuKnown = true;
      }
   }


   function handlePresentationKeyEvent(event) {
      // to make sure that a textarea element or input field can be used 
      // as expected (even if the event is "accidentally" propagated)
      var target = event.target.tagName;
      if (target === "TEXTAREA" || target === "INPUT") {
         return;
      }

      console.log("Key pressed: " + event.keyCode + ".");

      // When a modal dialog is open, deactivate keycode actions.
      if (isPrintDialogActive || isModalDialogActive) return;

      // Special Actions:
      //

      switch (event.keyCode) {
      case 72 /*h*/ :
      case 104 /*H*/ :
         toggleHelp();
         return;

      case 73 /*i*/ :
      case 105 /*I*/ :
         //toggleListOfSlides();
         toggleSubmenu('ldjs-menubar-submenu-list-of-slides',
            "ldjs-menubar-list-of-slides-button");
         return;

      case 66 /*b*/ :
      case 98 /*B*/ :
         toggleBlackout();
         return;

      case 87 /*w*/ :
         //toggleWhiteout();
         return;

      case 67 /*c*/ :
      case 99 /*C*/ :
         toggleCanvas();
         return;

      case 79 /*o*/ :
      case 108 /*O*/ :
         toggleScrollingOverview();
         return;
      }

      /*
       * Navigation of the slides
       */
      if (isOverlayActive) return;

      switch (event.keyCode) {
         // go back
      case 33 /*page up*/ :
      case 37 /*left arrow*/ :
      case 38 /*up arrow*/ :
         if (currentSlide > 0) {
            currentSlide -= 1;
         }
         break;

         // jump to a specific slide or advance to the next slide
      case 13 /*Enter*/ :
         if (enteredSlideNumber) {
            if (enteredSlideNumber > theSlides.length) {
               currentSlide = theSlides.length - 1;
            } else {
               currentSlide = enteredSlideNumber - 1;
            }
            break;
         }
         /* deliberately fall through */
         // advance
      case 32 /*Space*/ :
      case 34 /*page down*/ :
      case 39 /*right arrow*/ :
      case 40 /*down arrow*/ :
         if (currentSlide < theSlides.length) {
            currentSlide += 1;
         }
         break;

         // go to the first slide
      case 70 /*f*/ :
      case 102 /*F*/ :
         currentSlide = 0;
         break;

         // go to the last slide
      case 76 /*l*/ :
      case 108 /*L*/ :
         currentSlide = theSlides.length - 1;
         break;
         // Handle the case that a number is entered or that the keystroke has
         // no further meaning.
      default:
         if (event.keyCode >= 48 && event.keyCode <= 57) {
            if (enteredSlideNumber) {
               enteredSlideNumber = enteredSlideNumber * 10;
            } else {
               enteredSlideNumber = 0;
            }
            enteredSlideNumber += event.keyCode - 48;
         }
         return;
      }

      displaySlide();

   }

   function toggleCanvas() {
      var canvas = document.getElementById("ldjs-canvas");
      if (window.getComputedStyle(canvas).getPropertyValue("visibility") === "hidden") {
         canvas.style.visibility = "visible";
      } else {
         canvas.style.visibility = "hidden";
      }
   }

   function toggleHelp() {
      if (isHelpShown) {
         removeHelp();
      } else {
         addHelp();
      }
   }

   function toggleOverlayLayer(color) {
      // It is not possible to overlay the last slide stating that
      // the presentation has finished.
      if (currentSlide < theSlides.length) {
         if (isOverlayActive) {
            var overlayLayer = document.getElementById('ldjs-overlay-layer');
            if (overlayLayer.className != color) {
               overlayLayer.className = color;
            } else {
               removeOverlayLayer();
            }
         } else {
            addOverlayLayer(color);
         }
      }
   }

   function toggleBlackout() {
      toggleOverlayLayer('black');
   }

   function toggleWhiteout() {
      toggleOverlayLayer('white');
   }

   /**
    * Adds an onclick listener to jump to the provided slide number.
    * Also sets the slider number as HTML5 data "data-ldjs-slide"
    */

   function addSlideOnClickListener(element, slide) {
      element.setAttribute("data-ldjs-slide", slide);
      element.onclick = function (event) {
         var slide = parseInt(this.getAttribute("data-ldjs-slide"));
         jumpToSlide(slide)();
      };
   }

   /*	
    * Adds a styled overlay layer with the id "ldjs-overlay-layer" to the document.
    * @param {string} color the overlays base class name (intended to be synonymous to its color, e.g. white or black)
    * @param {string} [className] optional additional class(es) for the overlay
    */

   function addOverlayLayer(color, className) {
      var overlay = document.createElement("div");
      overlay.id = "ldjs-overlay-layer";
      overlay.classList.add(color);
      if (className) overlay.classList.add(className);
      overlay.onclick = bodyContent().onclick; // ensures that menu collapses when overlay is clicked
      document.body.appendChild(overlay);
      isOverlayActive = true;
   }

   function removeOverlayLayer() {
      document.body.removeChild(document.getElementById("ldjs-overlay-layer"));
      isOverlayActive = false;
   }

   function addMessageBox(id, innerHTML, onclose, timeout, className) {
      var messageBox = document.createElement("div");
      messageBox.id = id;
      messageBox.innerHTML = innerHTML;
      if (onclose) {
         messageBox.onclick = onclose;
         if (timeout) {
            timeouts.push(setTimeout(onclose, timeout));
         }
      } else {
         if (timeout) {
            timeouts.push(
               setTimeout(function () {
                     if (bodyContent().contains(messageBox)) {
                        bodyContent().removeChild(messageBox);
                     }
                  },
                  timeout)
            );
         }
      }
      messageBox.className = className || "ldjs-message ldjs-message-information";
      bodyContent().appendChild(messageBox);
   }

   function addHelp() {
      addMessageBox(
         "ldjs-help",
         helpNode.innerHTML,
         removeHelp,
         30000);
      isHelpShown = true;
   }

   function removeHelp() {
      var help = document.getElementById("ldjs-help");
      if (help) {
         bodyContent().removeChild(help);
         isHelpShown = false;
      }
   }

   function renderContinuousRepresentation() {
      clear();
      document.removeEventListener("keyup", handlePresentationKeyEvent, true);

      showMenuButton('ldjs-menubar-print-button');

      document.body.classList.add("ldjs-continuous-layout");

      for (var s = 0; s < theSlides.length; s++) {
         bodyContent().appendChild(theSlides[s]);
      }
   }

   function renderLightTable() {
      clear();
      document.removeEventListener("keyup", handlePresentationKeyEvent, true);

      showMenuButtons(['ldjs-menubar-zoom-button',
       'ldjs-menubar-print-button']);

      document.body.classList.add("ldjs-light-table");

      for (var s = 0; s < theSlides.length; s++) {
         var lightTableSlide = document.createElement("div");
         addSlideOnClickListener(lightTableSlide, s);
         lightTableSlide.className = "ldjs-light-table-slide";
         lightTableSlide.appendChild(theSlides[s].cloneNode(true));
         var slideNumberSpan = document.createElement("span");
         slideNumberSpan.className = "ldjs-light-table-slide-number";
         slideNumberSpan.appendChild(document.createTextNode(String(s + 1)));
         lightTableSlide.appendChild(slideNumberSpan);
         bodyContent().appendChild(lightTableSlide);
      }

      initLightTableMetrics();
      setLightTableZoomFactor(calculateZoomForSlidesPerLine(lightTableSlidesPerRow));
   }

   /**
    * Moves all ASIDE elements with a data-title attribute from the slide they are on to
    * the slide's parent element right behind the slide.
    * @param {Array of DOM elements} theSlides the list of slides to operate on
    */

   function moveAsides(theSlides) {
      theSlides.forEach(function (slide) {
         var slideParent = slide.parentNode;
         var toInsertBefore = slide.nextSibling;
         toArray(slide.querySelectorAll(".section-body>aside[data-title]")).forEach(
            function (aside) {
               slide.parentNode.insertBefore(aside, toInsertBefore);
            });
      });
   }

   function renderAsDocument() {
      clear();
      document.removeEventListener("keyup", handlePresentationKeyEvent, true);

      var contentClone = theDocumentBody.querySelector("#body-content").cloneNode(true);
      bodyContent().innerHTML = contentClone.innerHTML;

      var theSlides = toArray(document.querySelectorAll("section.slide"));
      addSlideNumbers(theSlides);

      hideAllMenuButtons();
      showDefaultMenuButtons();
      showMenuButtons(['ldjs-menubar-scrolling-overview-button',
       'ldjs-menubar-list-of-slides-button',
       'ldjs-menubar-print-button']);
      if (theSlides.length == 0) {
         disableButton("ldjs-menubar-modes-button");
         disableButton("ldjs-menubar-scrolling-overview-button");
      }
      if (theSlides.filter(function (s) {
         return s.dataset.title;
      }).length == 0) {
         disableButton("ldjs-menubar-list-of-slides-button");
      }

      moveAsides(theSlides);
   }

   function renderAsLectureNotes() {
      clear();
      document.removeEventListener("keyup", handlePresentationKeyEvent, true);

      document.body.classList.add("ldjs-lecture-notes");

      // The effect of the following JavaScript method could also easily
      // be achieved using, e.g., XSLT.
      // grab the id="body-content" element from the body's clone, since it's the only child of the body element
      var body = theDocumentBody.cloneNode(true).querySelector('body>div#body-content');

      // 1. remove any whitespace between the body tag 
      //    and the first non-text element
      while (body.hasChildNodes()) {
         if (body.firstChild.nodeType === Node.COMMENT_NODE ||
            (body.firstChild.nodeType === Node.TEXT_NODE && !/\S/.test(body.firstChild
               .data))) {
            body.removeChild(body.firstChild);
            console.log("Removed whitespace from the beginning of the document.");
         } else {
            break;
         }
      }

      // 2. create the lecture notes representation
      var lectureNotes = document.createElement("div");
      lectureNotes.id = "ldjs-lecture-notes";

      var subSection = document.createElement("div");
      subSection.className = "ldjs-lecture-notes-subsection";
      lectureNotes.appendChild(subSection);

      var sidebar;

      var content = document.createElement("div");
      content.className = "ldjs-lecture-notes-content";
      subSection.appendChild(content);

      while (body.hasChildNodes()) {
         var currentNode = body.firstChild;
         body.removeChild(currentNode);

         if (currentNode.tagName === 'SECTION') {
            if (!/slide/.test(currentNode.className)) { // currentNode.className !== "slide"){
               var nextNode = body.firstChild;
               while (currentNode.hasChildNodes()) {
                  var node = currentNode.firstChild;
                  currentNode.removeChild(node);
                  body.insertBefore(node, nextNode);
               }

               continue;
            }

            // we create a new section
            subSection = document.createElement("div");
            subSection.className = "ldjs-lecture-notes-subsection";
            lectureNotes.appendChild(subSection);

            sidebar = document.createElement("div");
            sidebar.className = "ldjs-lecture-notes-sidebar";
            subSection.appendChild(sidebar);
            sidebar.appendChild(currentNode);

            var sectionBody = currentNode.querySelector(
               "section.slide>div.section-body")
            if (sectionBody) {
               var asides = sectionBody.querySelectorAll("div.section-body>aside");
               for (var i = 0; i < asides.length; i++) {
                  var aside = asides[i];
                  sectionBody.removeChild(aside);
                  sidebar.appendChild(aside);
               }
            }

            content = document.createElement("div");
            content.className = "ldjs-lecture-notes-content";
            subSection.appendChild(content);
         } else {
            content.appendChild(currentNode);
         }
      }

      var footer = document.createElement("div");
      footer.id = "ldjs-lecture-notes-footer";
      footer.appendChild(document.createTextNode("Generated " + new Date()));

      bodyContent().appendChild(lectureNotes);
      bodyContent().appendChild(footer);
   }

   /* Helper functions for menu bar */

   /**
    * @return true if LectureDoc is currently in presentation mode
    */

   function isInPresentationMode() {
      return document.body.className.match(/\bldjs-slide\b/);
   }

   /**
    * @return true if LectureDoc is currently in light table mode
    */

   function isInLightTableMode() {
      return document.body.className.match(/\bldjs-light-table\b/);
   }

   /**
    * @return true if LectureDoc is currently in standard document mode
    */

   function isInStandardDocumentMode() {
      return !document.body.className;
   }

   /**
    * @return true if LectureDoc is currently in standard document mode
    */

   function isInContinuousLayoutMode() {
      return document.body.className.match(/\bldjs-continuous-layout\b/);
   }

   /**
    * Returns the absolute pixel coordinates on the page of the given element.<br>
    * "Absolute" here refers to the css position attribute "absolute", as in
    * absolute with respect to the current page.
    * @param element the element (DOM element)
    * @return an object with attributes top and left defining the element's coordinates
    */

   function getOffset(element) {
      var left = 0;
      var top = 0;
      while (element && !isNaN(element.offsetLeft) && !isNaN(element.offsetTop)) {
         left += element.offsetLeft;
         top += element.offsetTop;
         element = element.offsetParent;
      }
      return {
         top: top,
         left: left
      };
   }

   /**
    * @memberOf LectureDoc
    * @inner
    * Computes the given element's bounding box, consisting of its top left corner as
    * returned by getOffset as well as its width and height.
    * @param {DOM Element} element the element for which to compute the bounding box
    * @return {Object} the given elements bounding box.<br>
    * The box has the attributes top, left, width, and height.
    */

   function getBox(element) {
      var offset = getOffset(element);
      return {
         top: offset.top,
         left: offset.left,
         height: element.offsetHeight,
         width: element.offsetWidth
      };
   }

   /**
    * @memberOf LectureDoc
    * @inner
    * Determines if a DOM element is (partially) visible in the current viewport.
    * @param {DOM Element} element the element to check
    * @return {Boolean} true if the element is (partially) visible in the current viewport,
    * false if it resides entirely outside of the current viewport
    * @see getBox
    * @see isBoxInViewport
    */

   function isElementInViewport(element) {
      return isBoxInViewport(getBox(element));
   }

   /**
    * @memberOf LectureDoc
    * @inner
    * Determines if the given bounding box is (partially) visible in the current viewport.
    * @param {Object} box the box to check (for attributes @see getBox)
    * @return {Boolean} true if the element is (partially) visible in the current viewport,
    * false if it resides entirely outside of the current viewport
    */

   function isBoxInViewport(box) {
      return (box.top < (window.pageYOffset + window.innerHeight) &&
         box.left < (window.pageXOffset + window.innerWidth) &&
         (box.top + box.height) > window.pageYOffset &&
         (box.left + box.width) > window.pageXOffset);
   }

   /**
    * @memberOf LectureDoc
    * @inner
    * Finds the index of the first slide that is (partially) displayed in the current
    * viewport.<br>
    * @return {Number} in Presentation Mode, this method simply returns
    * {@linkcode #currentSlide}.
    * In any other mode, it returns the index of the first slide from the top left corner
    * that is at least partially in the current viewport. If, e.g. because of notes
    * between slides, there are no slides in the current viewport, this method returns
    * the index of the slide that precedes the current viewport.
    */

   function indexOfCurrentSlide() {
      if (isInPresentationMode()) {
         return currentSlide;
      } else {
         var theSlides = document.body.querySelectorAll("section.slide");
         for (var i = 0; i < theSlides.length; i++) {
            var box = getBox(theSlides[i]);
            if (isBoxInViewport(box)) {
               return i;
            } else if (box.top >= (window.pageYOffset + window.innerHeight)) {
               return i - 1;
            }
         }
         return 0;
      }
   }

   /* Functions pertaining to showing/hiding menubar and submenus */

   /**
    * Returns true if the given menu is visible.
    * @param menu the menu (DOM element)
    */

   function isMenuVisible(menu) {
      return menu !== undefined && menu.className.match(/\bvisible\b/);
   }

   /**
    * Hide the given submenu.<br>
    * Removes the menu's class "visible".
    * @param menu the menu (DOM element)
    */

   function hideSubmenu(menu) {
      menu.classList.remove("visible");
   }

   /**
    * Hides all submenus.<br>
    * Those are all elements satisfying the query #ldjs-menubar>.ldjs-menubar-submenu and
    * #ldjs-menubar-scrolling-overview.
    */

   function hideAllSubmenus() {
      var submenus = document.body.querySelectorAll(".ldjs-menubar-submenu");
      toArray(submenus).forEach(hideSubmenu);
      hideSubmenu(document.getElementById("ldjs-menubar-scrolling-overview"));
   }

   /**
    * Hides the menu bar and all submenus.
    * @see hideAllSubmenus
    */

   function hideMenubar() {
      hideAllSubmenus();
      var menu = document.getElementById("ldjs-menubar");
      menu.classList.remove("visible");
   }

   /**
    * Returns a function that executes the given action and hides the menu bar afterward.
    * @param action a function that takes no parameters
    */

   function hideMenubarAfterAction(action) {
      return function () {
         action();
         hideMenubar();
      }
   }

   function showMenubar() {
      var menu = document.getElementById("ldjs-menubar");
      menu.classList.add("visible");
   }

   /**
    * Toggles the menu bar's visibility.
    */

   function toggleMenubar() {
      var menu = document.getElementById("ldjs-menubar");
      if (isMenuVisible(menu)) {
         hideMenubar();
      } else {
         showMenubar();
      }
   }

   /**
    * Computes the value for the style attribute left that in conjunction with fixed
    * positioning will result in the menu being centered horizontally over the given
    * element.<br>
    * If the menu would overlap the right edge of the screen, it will be moved far enough
    * to the left so that it will be completely visible (given that its width is less
    * than the screen width).
    * @param menu the menu (DOM element)
    * @param element the element to center over (DOM element)
    */

   function centerHorizontallyOverElement(menu, element) {
      var left = getOffset(element).left;
      left -= Math.ceil((menu.offsetWidth - element.offsetWidth) / 2);
      var right = left + menu.offsetWidth;
      var difference = Math.ceil(right - window.innerWidth);
      if (difference > 0) {
         left -= difference;
      }
      menu.style.left = left + "px";
   }

   /**
    * Shows the given submenu.<br>
    * It will be centered over the button with the given id.
    * @param menu the menu (DOM element)
    * @param buttonId the id of the button to center over (string)
    * @param {Boolean} [doShowMenubar=true] true to show the menubar, false to leave
    * it invisible.
    */

   function showSubmenu(menu, buttonId, doShowMenubar) {
      hideAllSubmenus();
      if (doShowMenubar || doShowMenubar === undefined) showMenubar();
      menu.classList.add("visible");
      var button = document.getElementById(buttonId);
      centerHorizontallyOverElement(menu, button);
   }

   /**
    * Toggles the visibility of the submenu with the given id.<br>
    * @see showSubmenu(menu, buttonId)
    * @param id the menu's id (string)
    * @param buttonId the id of the button that the menu will be centered over (string)
    */

   function toggleSubmenu(id, buttonId) {
      var menu = document.getElementById(id);
      if (isMenuVisible(menu)) {
         hideSubmenu(menu);
      } else {
         showSubmenu(menu, buttonId);
      }
   }

   /**
    * Returns a function that calls toggleSubmenu with the given parameters.<br>
    * For use as an onclick handler for menu buttons.
    * @param id the id of the menu to toggle (string)
    * @param buttonId the id of the button to center the menu over (string)
    * @see toggleSubmenu(id, buttonId)
    */

   function toggleSubmenuHandler(id, buttonId) {
      return function () {
         toggleSubmenu(id, buttonId);
      }
   }

   /**
    * Toggles visibility of the scrolling overview.
    */

   function toggleScrollingOverview() {
      var overview = document.getElementById("ldjs-menubar-scrolling-overview");
      if (isMenuVisible(overview)) {
         hideSubmenu(overview);
      } else {
         showMenubar();
         hideAllSubmenus();
         var currentSlideIndex = indexOfCurrentSlide();
         var currentSlide = overview.querySelectorAll("section.slide")[
            currentSlideIndex];
         overview.querySelector("#current-slide-highlight").style.left = currentSlide.style
            .left;
         scrollOverviewToSlide(currentSlideIndex);
         overview.classList.add("visible");
      }
   }

   function toggleAsidesMenu() {
      var menu = document.getElementById("ldjs-menubar-submenu-asides");
      if (isMenuVisible(menu)) {
         hideSubmenu(menu);
      } else {
         hideAllSubmenus();
         menu.innerHTML = ""; // remove all children
         slideAsides[currentSlide].forEach(function (link) {
            menu.appendChild(wrapInLi(link));
         });
         showSubmenu(menu, "ldjs-menubar-asides-button", false);
      }
   }

   /* Functions pertaining to enabling/disabling and showing/hiding buttons */

   /**
    * Sets the disabled class on the button with the given id.
    * @param id the id of the button to be disabled (string)
    */

   function disableButton(id) {
      var button = document.getElementById(id);
      button.classList.add("disabled");
   }

   /**
    * Removes the disabled class from the button with the given id.
    * @param id the id of the button to be enabled (string)
    */

   function enableButton(id) {
      var button = document.getElementById(id);
      button.classList.remove("disabled");
   }

   /**
    * Removes the hidden class from the button with the given id.
    * @param id the id of the button to be made visible (string)
    */

   function showMenuButton(id) {
      var button = document.getElementById(id);
      button.classList.remove("hidden");
   }

   /**
    * Executes showMenuButton for all buttons in the given list.
    * @param idList a list of ids of the buttons to be shown (Array of strings)
    */

   function showMenuButtons(idList) {
      idList.forEach(showMenuButton);
   }

   /**
    * Executes showMenuButton for those buttons that should be visible in all modes.
    */

   function showDefaultMenuButtons() {
      showMenuButtons(["ldjs-menubar-toggle-button",
                   "ldjs-menubar-help-button",
                   "ldjs-menubar-modes-button"]);
   }

   /**
    * Hides all menu bar buttons (the elements found by the query #ldjs-menubar>a).
    */

   function hideAllMenuButtons() {
      var buttons = toArray(document.querySelectorAll('#ldjs-menubar>a'));
      buttons.forEach(function (b) {
         b.classList.add("hidden")
      });
   }

   /* Functions pertaining to creating menu elements */

   /**
    * Creates a menu button (an A element) with the class "ldjs-menubar-button" for use
    * in the menu bar.<br>
    * If an icon is specified, the button will have the class "ldjs-menubar-icon-button").
    * @param id the button's id (string)
    * @param text the button's text (string; can be empty)
    * @param icon the name of the icon to be used for the button (string; can be empty)
    * <br> the name is treated as part of the file name:
    * ("Library/LectureDoc/MenuIcons/" + icon + ".svg")
    * @param action the button's onclick action
    */

   function createMenuButton(id, text, icon, action) {
      var button = document.createElement("a");
      button.appendChild(document.createTextNode(text));
      button.addEventListener("click", action, false);
      button.id = id;
      button.classList.add("ldjs-menubar-button");
      if (icon !== '') {
         var iconsFolder = "Library/LectureDoc/MenuIcons/";
         button.style.backgroundImage = 'url("' + iconsFolder + icon + '.svg")';
         button.classList.add("ldjs-menubar-icon-button");
      }
      return button;
   }

   /**
    * Wraps the given element into an LI element.
    * @param element the element (DOM element)
    * @returns an LI element containing the given element as its only child
    */

   function wrapInLi(element) {
      var li = document.createElement("li");
      li.appendChild(element);
      return li;
   }

   /**
    * Creates a submenu (UL or OL element with the class ldjs-menubar-submenu).
    * @param id the submenu's id (string)
    * @param ordered true to create an OL, false for a UL (boolean)
    */

   function createSubmenu(id, ordered) {
      var element = document.createElement(ordered ? 'ol' : 'ul');
      element.id = id;
      element.className = 'ldjs-menubar-submenu';
      return element;
   }

   function createBlackoutButton() {
      return createMenuButton('ldjs-menubar-blackout-button', '', 'blackout',
         hideMenubarAfterAction(toggleBlackout));
   }

   function createWhiteoutButton() {
      return createMenuButton('ldjs-menubar-whiteout-button', '', 'whiteout',
         hideMenubarAfterAction(toggleWhiteout));
   }

   function createCanvasButton() {
      return createMenuButton('ldjs-menubar-canvas-button', '', 'canvas',
         hideMenubarAfterAction(toggleCanvas));
   }

   function createListOfSlidesMenu() {
      var menu = createSubmenu('ldjs-menubar-submenu-list-of-slides', true);
      var buttonAction = function (s) {
         return hideMenubarAfterAction(jumpToSlide(s))
      };

      for (var s = 0; s < theSlides.length; s++) {
         var title = theSlides[s].dataset.title;
         if (title) {
            menu.appendChild(wrapInLi(createMenuButton('', title, '',
               buttonAction(s))));
         }
      }

      return menu;
   }

   function createListOfSlidesButton() {
      var id = "ldjs-menubar-list-of-slides-button";
      return createMenuButton(id, "", "list-of-slides",
         toggleSubmenuHandler("ldjs-menubar-submenu-list-of-slides", id));
   }

   /**
    * Scrolls the given element by the specified amount, using its scrollLeft property.
    * @param element the element (DOM element)
    * @param amount the amount to scroll ( < 0 to the left, > 0 to the right )
    */

   function scroll(element, amount) {
      element.scrollLeft += amount;
   }

   function createScrollingOverview() {
      var overview = document.createElement("div");
      overview.id = "ldjs-menubar-scrolling-overview";

      document.body.appendChild(overview);

      for (var i = 0, left = 0; i < theSlides.length; i++) {
         var clone = theSlides[i].cloneNode(true);
         clone.style.left = left + "px";
         clone.addEventListener("click", hideMenubarAfterAction(jumpToSlide(i)));
         overview.appendChild(clone);
         left += clone.offsetWidth;
      }
      var highlight = document.createElement("div");
      highlight.innerHTML = " ";
      highlight.id = "current-slide-highlight";
      overview.appendChild(highlight);

      var scrollIntervalHandler = undefined;
      var scrollInterval = 50; // milliseconds
      // speeds in pixels
      var minimumSpeed = 2;
      var maximumSpeed = 50;

      var leftRegion = 1 / 3;
      var rightRegion = 1 - leftRegion;

      /*
       * Scrolling speed is calculated with the inner edge of the scrolling area mapped
       * to minimumSpeed and the outer edge mapped to maximumSpeed.
       * The variables base and stretch are used in the conversion; they are obtained
       * as:
       * 1 - leftRegion * stretch + base = minimumSpeed
       * 1 * stretch + base = maximumSpeed
       */
      var base = (minimumSpeed - rightRegion * maximumSpeed) / leftRegion;
      var stretch = maximumSpeed - base;

      /**
       * Converts the given position ratio to a scroll amount.
       * @param {Number} ratio is expected to be between 0 (left edge) and 1 (right edge)
       * @return the amount of pixels to scroll, regardless of direction<br>
       */

      function ratioToAmount(ratio) {
         if (ratio < 0.5) ratio = 1 - ratio; // normalize to (1-leftRegion) .. 1
         return ratio * stretch + base;
      }

      /**
       * Terminates the scrolling interval registered under the scrollIntervalHandler
       * variable.
       */

      function stopScrolling() {
         if (scrollIntervalHandler !== undefined) {
            clearInterval(scrollIntervalHandler);
         }
      }

      /**
       * Repeatedly scrolls the given element by the specified amount; the repetition
       * interval is given in the variable scrollInterval. Halts previous scrolling
       * prior to registering its own scrolling interval.
       * @param element the element (DOM element)
       * @param amount the amount to scroll ( < 0 to the left, > 0 to the right )
       */

      function scrollRepeat(element, amount) {
         stopScrolling();
         scroll(element, amount);
         scrollIntervalHandler = setInterval(function () {
            scroll(element, amount);
         }, scrollInterval);
      }

      function startScrolling(event) {
         var width = overview.clientWidth;
         var x = event.pageX;
         var ratio = x / width;
         var amount = ratioToAmount(ratio);
         if (ratio <= leftRegion) {
            scrollRepeat(overview, -amount);
         } else if (ratio >= rightRegion) {
            scrollRepeat(overview, amount);
         } else {
            stopScrolling();
         }
      }
      overview.addEventListener("mousemove", startScrolling, false);
      overview.addEventListener("mouseout", stopScrolling, false);

      return overview;
   }

   /**
    * @memberOf LectureDoc
    * @inner
    * Scrolls the overview so that the given slide is as close to the center of the
    * overview as possible.
    * @param {Number} slide the slide to put in the center
    */

   function scrollOverviewToSlide(slide) {
      var overview = document.getElementById("ldjs-menubar-scrolling-overview");
      var overviewSlideWidth = overview.querySelector("section.slide").offsetWidth;
      var leftForCenter = -(overview.offsetWidth - overviewSlideWidth) / 2;
      var slideLeft = overviewSlideWidth * slide;
      var targetLeft = slideLeft + leftForCenter;
      overview.scrollLeft = targetLeft;
   }

   function createScrollingOverviewButton() {
      var id = "ldjs-menubar-scrolling-overview-button";
      return createMenuButton(id, "", "overview", toggleScrollingOverview);
   }

   function createPrintButton() {
      return createMenuButton('ldjs-menubar-print-button', '', 'print',
         hideMenubarAfterAction(function () {
            openPrintDialog();
         }));
   }

   function createHelpButton() {
      return createMenuButton('ldjs-menubar-help-button', '', 'help',
         hideMenubarAfterAction(toggleHelp));
   }

   function createModesMenu() {
      var modes = createSubmenu('ldjs-menubar-submenu-modes', false);
      modes.appendChild(wrapInLi(createMenuButton('ldjs-modes-presentation-button',
         'Presentation Mode', '', hideMenubarAfterAction(displaySlide))));
      modes.appendChild(wrapInLi(createMenuButton(
         'ldjs-modes-standard-document-layout-button',
         'Standard Mode', '', hideMenubarAfterAction(renderAsDocument))));
      modes.appendChild(wrapInLi(createMenuButton('ldjs-modes-lecture-notes-button',
         'Compact Mode', '', hideMenubarAfterAction(renderAsLectureNotes))));
      modes.appendChild(wrapInLi(createMenuButton('ldjs-modes-light-table-button',
         'Light Table', '', hideMenubarAfterAction(renderLightTable))));
      // TODO remove this mode if printing in Light Table Fit-to-Page works reliably
      modes.appendChild(wrapInLi(createMenuButton('ldjs-modes-continuous-button',
         'Continuous', '', hideMenubarAfterAction(renderContinuousRepresentation)
      )));
      return modes;
   }

   function createModesButton() {
      var id = "ldjs-menubar-modes-button";
      return createMenuButton(id, '', 'modes',
         toggleSubmenuHandler("ldjs-menubar-submenu-modes", id));
   }

   /**
    * Compares the size of the current slide and the window and, if the slide is bigger
    * than the window, activates scrollbars.
    */

   function manageScrollbars() {
      if (!isInPresentationMode()) return;
      var slide = document.querySelector('section.slide');
      if (slide === null) return;
      // here we use +1 to circumvent browser miscalculation (firefox appears to do some)
      // e.g. in 1024x768 resolution and full screen, it has an innerHeight of 767 pixels
      var wider = slide.clientWidth > window.innerWidth + 1;
      var higher = slide.clientHeight > window.innerHeight + 1;
      if (wider || higher) {
         document.body.classList.add("scroll");
      } else {
         document.body.classList.remove("scroll");
      }
   }

   var metrics = {
      slideWidth: 0,
      slideHeight: 0,
      slideAspectRatio: 0,
      lightTableSlideNumberHeight: 0,
      lightTableSlideContainerPadding: 0
   }

      function initLightTableMetrics() {
         if (metrics.lightTableSlideContainerPadding === 0) {
            var slide = document.querySelector(".ldjs-light-table-slide");
            metrics.lightTableSlideContainerPadding =
               parseInt(getComputedStyle(slide).paddingLeft);
         }
         if (metrics.lightTableSlideNumberHeight === 0) {
            var slideNumber = document.querySelector(".ldjs-light-table-slide-number");
            metrics.lightTableSlideNumberHeight = slideNumber.offsetHeight;
         }
      }

      function initSlideMetrics() {
         var aSlideBody = document.querySelector("section.slide>.section-body");
         if (aSlideBody == null) return;
         if (metrics.slideWidth === 0) {
            metrics.slideWidth = aSlideBody.offsetWidth;
         }
         if (metrics.slideHeight === 0) {
            metrics.slideHeight = aSlideBody.offsetHeight;
         }
         if (metrics.slideAspectRatio === 0) {
            metrics.slideAspectRatio = metrics.slideWidth / metrics.slideHeight;
         }
      }

      /**
       * Applies the given zoom factor on the presentation mode.
       * @param factor the zoom factor (1.0 == 100% == full presentation mode slide width/
       * height)
       */

      function setPresentationZoomFactor(factor) {
         var transform = 'scale(%zoom,%zoom)'.replace(/%zoom/g, factor);
         var width = (metrics.slideWidth * factor) + 'px';
         var height = (metrics.slideHeight * factor) + 'px';
         var slide = document.querySelector('section.slide');
         slide.style.width = width;
         slide.style.height = height;
         var slideBody = slide.querySelector('div.section-body');
         slideBody.style.transform = transform;
         slideBody.style.WebkitTransform = transform;
         slideBody.style.transformOrigin = '0 0';
         slideBody.style.WebkitTransformOrigin = '0 0';
         document.body.style.minWidth = width;
         bodyContent().dataset.ldjsSlideZoom = factor;
         manageScrollbars();
      }

      /**
       * Applies the given zoom factor on the light table mode.
       * @param factor the zoom factor (1.0 == 100% == full presentation mode slide width/
       * height)
       */

      function setLightTableZoomFactor(factor) {
         var transform = 'scale(%zoom,%zoom)'.replace(/%zoom/g, factor);
         var width = (metrics.slideWidth * factor) + 'px';
         var height = (metrics.slideHeight * factor) + 'px';
         var slides = document.querySelectorAll('.ldjs-light-table-slide');
         for (var i = 0; i < slides.length; i++) {
            var slide = slides[i];
            slide.style.width = width;
            var section = slide.querySelector('section.slide');
            section.style.width = width;
            section.style.height = height;
            var slideBody = section.querySelector('div.section-body');
            slideBody.style.transform = transform;
            slideBody.style.WebkitTransform = transform;
         }
      }

      /**
       * Resets the zoom level to the mode specific default (presentation and light table
       * mode).
       * Actually just clears all direct slide styles pertaining to scaling.
       */

      function resetZoom() {
         var mode = document.body.className;
         if (isInPresentationMode()) {
            var slide = document.querySelector('section.slide');
            if (slide === null) return;
            slide.style.width = '';
            slide.style.height = '';
            var slideBody = slide.querySelector('div.section-body');
            slideBody.style.transform = '';
            slideBody.style.WebkitTransform = '';
            slideBody.style.TransformOrigin = '';
            slideBody.style.WebkitTransformOrigin = '';
            document.body.style.minWidth = '';
            manageScrollbars();
         } else if (mode === 'ldjs-light-table') {
            var slides = document.querySelectorAll('.ldjs-light-table-slide');
            for (var i = 0; i < slides.length; i++) {
               var slide = slides[i];
               slide.style.width = '';
               var section = slide.querySelector('section.slide');
               section.style.width = '';
               section.style.height = '';
               var slideBody = section.querySelector('div.section-body');
               slideBody.style.transform = '';
               slideBody.style.WebkitTransform = '';
            }
         }
      }

      /**
       * Calculates the zoom factor necessary to display the current slide as big as
       * possible without cutting off anything.
       */

      function fitPresentationSlideToPage() {
         var windowWidth = window.innerWidth;
         var windowHeight = window.innerHeight;
         var zoom = 1;
         var windowResolution = windowWidth / windowHeight;
         if (windowResolution >= metrics.slideAspectRatio) {
            zoom = windowHeight / metrics.slideHeight;
         } else {
            zoom = windowWidth / metrics.slideWidth;
         }
         setPresentationZoomFactor(zoom);
      }

      function createPresentationZoomMenu() {
         var menu = createSubmenu('ldjs-menubar-submenu-presentation-zoom', false);
         var zoom = function (factor) {
            return function () {
               setPresentationZoomFactor(factor);
            };
         };
         var setZoomFactorAction = function (factor) {
            return hideMenubarAfterAction(zoom(factor));
         }

            function addZoomButton(level) {
               menu.appendChild(wrapInLi(createMenuButton("", level * 100 + "%", "",
                  setZoomFactorAction(level))));
            }
  [3, 2.5, 2, 1.5, 1, 0.75, 0.5].forEach(addZoomButton);
         menu.appendChild(wrapInLi(createMenuButton("", "Fit to Page", "",
            hideMenubarAfterAction(fitPresentationSlideToPage))));
         return menu;
      }


      /**
       * Calculates the zoom factor required to fit the given number of slides on one line.
       * @param slidesPerLine the number of slides (int)
       * @return the zoom factor
       */

      function calculateZoomForSlidesPerLine(slidesPerLine) {
         bodyContent().style.overflow = "scroll";
         var padding = (2 * metrics.lightTableSlideContainerPadding);
         var zoomedWidth = (bodyContent().offsetWidth / slidesPerLine) - padding;
         bodyContent().style.overflow = "";
         var zoom = zoomedWidth / metrics.slideWidth;
         return Math.floor(zoom * 1000) / 1000;
      }

      /**
       * Calculates the height required when the given number of slides are fitted on one line.
       * @param slidesPerLine the number of slides (int)
       * @return the height (in pixels) that the element containing the slides would have
       */

      function calculateHeightForSlidesPerLine(slidesPerLine) {
         var numSlides = theSlides.length;
         var zoom = calculateZoomForSlidesPerLine(slidesPerLine);
         var padding = (2 * metrics.lightTableSlideContainerPadding);
         var height = (metrics.slideHeight * zoom);
         height += metrics.lightTableSlideNumberHeight + padding;
         var lines = Math.ceil(numSlides / slidesPerLine);
         return height * lines;
      }

      /**
       * Calculates (and applies) the zoom factor necessary to display all slides
       * on the light table page without cutting off anything.
       */

      function fitAllOnLightTablePage() {
         var numSlides = theSlides.length;
         var zoom = 0;
         for (var slides = 0;; slides++) {
            var height = calculateHeightForSlidesPerLine(slides);
            if (height == 0) {
               zoom = calculateZoomForSlidesPerLine(slides - 1);
               lightTableSlidesPerRow = slides - 1;
               break;
            }
            if (height <= window.innerHeight) {
               zoom = calculateZoomForSlidesPerLine(slides);
               lightTableSlidesPerRow = slides;
               break;
            }
         }
         setLightTableZoomFactor(zoom);
      }

      function createLightTableZoomMenu() {
         var menu = createSubmenu('ldjs-menubar-submenu-light-table-zoom', false);
         var setZoomFactorAction = function (slides) {
            return hideMenubarAfterAction(function () {
               var zoom = calculateZoomForSlidesPerLine(slides);
               setLightTableZoomFactor(zoom);
               lightTableSlidesPerRow = slides;
            });
         };
         menu.appendChild(wrapInLi(createMenuButton("", "Show as many as possible", "",
            hideMenubarAfterAction(fitAllOnLightTablePage))));

         function addZoomButton(slides) {
            menu.appendChild(wrapInLi(createMenuButton("", slides + " per row", "",
               setZoomFactorAction(slides))));
         }
  [10, 9, 8, 7, 6, 5, 4, 3, 2, 1].forEach(addZoomButton)
         return menu;
      }

      function createZoomButton() {
         var buttonId = "ldjs-menubar-zoom-button";
         return createMenuButton(buttonId, "", "zoom", function () {
            if (isInPresentationMode()) {
               toggleSubmenu("ldjs-menubar-submenu-presentation-zoom", buttonId);
            } else {
               toggleSubmenu("ldjs-menubar-submenu-light-table-zoom", buttonId);
            }
         });
      }

      function createAsidesButton() {
         return createMenuButton("ldjs-menubar-asides-button", "", "nav",
            toggleAsidesMenu);
      }

      function createAsidesMenu() {
         return createSubmenu("ldjs-menubar-submenu-asides", false);
      }

      function createToggleButton() {
         return createMenuButton('ldjs-menubar-toggle-button', '', 'arrow',
            toggleMenubar);
      }

      /**
       * Creates a separator element, floating to the given orientation (left or right).
       * @param orientation left or right; defaults to right (string)
       * @return a span element with class names "separator" and orientation parameter
       */

      function createSeparator(orientation) {
         var separator = document.createElement("span");
         separator.className = "separator " + (orientation || "right");
         return separator;
      }

      function addMenubar() {
         var menubar = document.createElement("nav");
         menubar.id = "ldjs-menubar";
         menubar.appendChild(createToggleButton());
         menubar.appendChild(createAsidesButton());
         menubar.appendChild(createSeparator());

         menubar.appendChild(createModesButton());
         menubar.appendChild(createZoomButton());
         menubar.appendChild(createScrollingOverviewButton());
         menubar.appendChild(createListOfSlidesButton());
         menubar.appendChild(createCanvasButton());
         menubar.appendChild(createBlackoutButton());
         menubar.appendChild(createWhiteoutButton());
         menubar.appendChild(createHelpButton());
         menubar.appendChild(createPrintButton());
         menubar.appendChild(createSeparator("left"));
         menubar.appendChild(createModesMenu());
         menubar.appendChild(createScrollingOverview());
         menubar.appendChild(createPresentationZoomMenu());
         menubar.appendChild(createLightTableZoomMenu());
         menubar.appendChild(createListOfSlidesMenu());
         menubar.appendChild(createAsidesMenu());
         document.body.appendChild(menubar);
         hideAllSubmenus();
         bodyContent().addEventListener("click", function () {
            hideMenubar();
         }, false);
      }

      /**
       * Adds a pair consisting of a dt and a dd element to the given dl.
       * @param {DOM node} definitionList the dl to add to
       * @param {string} term the definition term
       * @param {Array of string} definitionFragments will be concatenated with ", "
       */

      function addDefinition(definitionList, term, definitionFragments) {
         var defTerm = document.createElement("dt");
         defTerm.appendChild(document.createTextNode(term));
         definitionList.appendChild(defTerm);

         var defDescription = document.createElement("dd");
         defDescription.innerHTML = definitionFragments.join(", ");
         definitionList.appendChild(defDescription);
      }

      /**
       * Returns the date of last modification as stored in the document.<br>
       * If the document defines no such date, returns null.
       */

      function getLastModifiedDate() {
         if (document.body.dataset.ldjsLastModified) {
            return new Date(Number(document.body.dataset.ldjsLastModified));
         }
         return null;
      }

      /**
       * Constructs the LectureDoc help from the given helpData object and stores
       * it in the helpNode field.
       * @see LectureDoc.setHelp for the required fields of the helpData object
       */

      function buildHelpNode(helpData) {
         helpNode = document.createElement("div");
         helpNode.id = "ldjs-help";

         var headerNode = document.createElement("header");
         var header = helpData.header || "Help";
         headerNode.innerHTML = header;

         helpNode.appendChild(headerNode);

         var help = helpData.content;
         if (typeof (help) == "string") {
            var p = document.createElement('p');
            p.appendChild(document.createTextNode(help));
            helpNode.appendChild(p);
         } else {
            var definitionList = document.createElement("dl");
            for (var group in help) {
               for (var key in help[group]) {
                  addDefinition(definitionList, key, help[group][key]);
               }
               addDefinition(definitionList, '', ['&nbsp;']);
            }
            helpNode.appendChild(definitionList);
         }
         var lastModifiedSpan = document.createElement("span");
         lastModifiedSpan.style.fontWeight = "bold";
         var helpFooter = document.createElement("footer");
         var p = document.createElement('p');
         var lastModifiedDate = getLastModifiedDate();
         if (lastModifiedDate instanceof Date) {
            p.appendChild(document.createTextNode(
               "This document was last modified on "));
            p.appendChild(lastModifiedSpan);
            lastModifiedSpan.appendChild(document.createTextNode(lastModifiedDate.toLocaleString()));
         } else {
            p.appendChild(document.createTextNode("Modification date not available."));
         }
         helpFooter.appendChild(p);
         var footer = helpData.footer ||
            "LectureDoc © 2013 Michael Eichberg, Marco Jacobasch, Arne Lottmann, Daniel Killer, Simone Wälde, Kerstin Reifschläger";
         p = document.createElement('p');
         p.innerHTML = footer;
         helpFooter.appendChild(p);

         helpNode.appendChild(helpFooter);
      }

   return { // The LectureDoc Object
      init: function () {
         analyzeDocument();
         enableLectureDoc();
         initSlideMetrics();
         addMenubar();
         window.addEventListener("resize", manageScrollbars, false);

         try {
            var storageItem = window.localStorage.getItem("lecturedoc-currentSlide-" +
               window.location.toString())
            if (storageItem !== null) {
               var lastShownSlide = parseInt(storageItem);
               console.log("Setting up presentation with last shown slide: " +
                  lastShownSlide);
               setupPresentationMode(lastShownSlide);
            } else {
               console.log("Setting up presentation with the first slide.");
               setupPresentationMode(0);
            }
         } catch (err) {
            console.log('Using "local storage" is not supported: ' + err + ".");
            setupPresentationMode(0);
         }

         renderAsDocument();
         showMenubar();
      },

      /**
       * Sets LectureDoc's help message.
       * @param helpData a data object that has to specify the following fields
       * <ul>
       * <li>header : a plain HTML string to be used as the message header;
       * optional, defaults to "Help".<br>
       * It will be inserted into the message's HEADER tag.</li>
       * <li>content : a list of objects that represent logical groups of
       * key/value pairs, for example action description and shortcut
       * key.</li>
       * <li>footer : a plain HTML string to be used as the message footer;
       * optional, defaults to "LectureDoc © 2013 Michael Eichberg et al."<br>
       * It will be inserted into a P tag in the message's FOOTER tag.</li>
       * </ul>
       */
      setHelp: function (helpData) {
         buildHelpNode(helpData);
      }
   };
}());

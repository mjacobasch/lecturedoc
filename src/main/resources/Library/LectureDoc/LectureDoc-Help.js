LectureDoc.setHelp({
   header: "Help",
   content: [
      {
         "toggle this help": ["h"],
         "select rendering mode": ["m"]
  },
      {
         "first slide": ["f"],
         "last slide": ["l"],
         "next slide": ["space", "enter", "right", "down", "page down"],
         "previous slide": ["left", "up", "page up"]
  },
      {
         "list of slides": ["i"],
         "overview": ["o"],
         "go to slide": ["[0...9]<sup>*</sup> + enter"]
  },
      {
         "black out": ["b"],
         "virtual laser pointer": ["ctrl + &lt;mouse movement&gt;"],
         "(de-)activates canvas": ["c"],
         "draws line if canvas is active": ["alt + &lt;mouse movement&gt;"]
  }
 ],
   footer: "LectureDoc © 2013 Michael Eichberg, Marco Jacobasch, Arne Lottmann, Daniel Killer, Simone Wälde, Kerstin Reifschläger"
});

name := "LectureDoc"

version := "0.0.0"

scalaVersion := "2.10.2"

scalacOptions in (Compile,compile) += "–deprecation" 

scalacOptions in (Compile,compile) += "–target:jvm-1.7" 
 
scalacOptions in Compile += "-feature" 

libraryDependencies += "junit" % "junit" % "4.10" % "test"

libraryDependencies += "org.scalatest" % "scalatest_2.10" % "1.9.1" % "test"

seq(com.github.retronym.SbtOneJar.oneJarSettings: _*)

mainClass in oneJar := Some("de.lecturedoc.tools.LectureDoc")
